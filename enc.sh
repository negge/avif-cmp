#!/bin/bash

set -e

LIBAOM=~/git/libaom.td/aom_build/
LIBAOM_SVC=~/git/libaom.td/aom_build_svc/

HDRTOOLS=~/git/HDRTools.new/bin

LIBVMAF=~/git/vmaf.new/libvmaf/build/tools

# Use first parameter as image
FILE=$1

WIDTH=$(head -1 $FILE | tr ' ' '\n' | grep -E '\bW' | tr -d 'W')
HEIGHT=$(head -1 $FILE | tr ' ' '\n' | grep -E '\bH' | tr -d 'H')
CHROMA=$(head -1 $FILE | tr ' ' '\n' | grep -E '\bC')
DEPTH=8
case $CHROMA in
C444p10 | C420p10)
  DEPTH=10
  ;;
esac

BASENAME=$(basename $FILE)

echo $BASENAME

QP0=32
QP1=32
QP2=32

$LIBAOM/examples/svc_encoder_rtc $FILE $BASENAME av1 $WIDTH $HEIGHT 1 24 0 0 1 1 6 $QP0 $QP1 $QP2 > /dev/null
$LIBAOM/aomdec $BASENAME?0.av1 -o $BASENAME-0.y4m > /dev/null
$LIBAOM/aomdec $BASENAME?1.av1 -o $BASENAME-1.y4m > /dev/null
$LIBAOM/aomdec $BASENAME?2.av1 -o $BASENAME-2.y4m > /dev/null

WIDTH0=$(head -1 $BASENAME-0.y4m | tr ' ' '\n' | grep -E '\bW' | tr -d 'W')
HEIGHT0=$(head -1 $BASENAME-0.y4m | tr ' ' '\n' | grep -E '\bH' | tr -d 'H')
CHROMA0=$(head -1 $BASENAME-0.y4m | tr ' ' '\n' | grep -E '\bC')

# layer 0

echo "YUV4MPEG2 W$WIDTH0 H$HEIGHT0 F30:1 $CHROMA0" > $BASENAME-0-$$.y4m
$(tail -n+2 $BASENAME-0.y4m >> $BASENAME-0-$$.y4m)

$HDRTOOLS/HDRConvert -p SourceFile=$BASENAME-0-$$.y4m -p OutputFile=$BASENAME-0-up.y4m   -p OutputWidth=$WIDTH -p OutputHeight=$HEIGHT -p OutputChromaFormat=1 -p OutputBitDepthCmp0=$DEPTH -p OutputBitDepthCmp1=$DEPTH -p OutputBitDepthCmp2=$DEPTH -p OutputColorSpace=0 -p OutputColorPrimaries=0 -p OutputTransferFunction=12 -p SilentMode=1 -p ScaleOnly=1 -p ScalingMode=7 -p OutputRate=24 -p NumberOfFrames=1 > /dev/null

WIDTH1=$(head -1 $BASENAME-1.y4m | tr ' ' '\n' | grep -E '\bW' | tr -d 'W')
HEIGHT1=$(head -1 $BASENAME-1.y4m | tr ' ' '\n' | grep -E '\bH' | tr -d 'H')
CHROMA1=$(head -1 $BASENAME-1.y4m | tr ' ' '\n' | grep -E '\bC')

# layer 1

echo "YUV4MPEG2 W$WIDTH1 H$HEIGHT1 F30:1 $CHROMA1" > $BASENAME-1-$$.y4m
$(tail -n+2 $BASENAME-1.y4m >> $BASENAME-1-$$.y4m)

$HDRTOOLS/HDRConvert -p SourceFile=$BASENAME-1-$$.y4m -p OutputWidth=$WIDTH -p OutputHeight=$HEIGHT -p OutputChromaFormat=1 -p OutputBitDepthCmp0=$DEPTH -p OutputBitDepthCmp1=$DEPTH -p OutputBitDepthCmp2=$DEPTH -p OutputColorSpace=0 -p OutputColorPrimaries=0 -p OutputTransferFunction=12 -p SilentMode=1 -p ScaleOnly=1 -p ScalingMode=7 -p OutputRate=24 -p NumberOfFrames=1 -p OutputFile=$BASENAME-1-up.y4m > /dev/null

# Compute metrics

$LIBVMAF/vmaf -d $BASENAME-0-up.y4m -r $FILE -q --aom_ctc v1.0 -o $BASENAME-vmaf-sr-0.xml --xml
$LIBVMAF/vmaf -d $BASENAME-1-up.y4m -r $FILE -q --aom_ctc v1.0 -o $BASENAME-vmaf-sr-1.xml --xml
$LIBVMAF/vmaf -d $BASENAME-2.y4m -r $FILE -q --aom_ctc v1.0 -o $BASENAME-vmaf-sr-2.xml --xml

SIZE0=$(wc -c $BASENAME?0.av1 | awk '{ print $1 }')
PSNR0=$(cat $BASENAME-vmaf-sr-0.xml | grep metric\ name\=\"psnr_y\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
SSIM0=$(cat $BASENAME-vmaf-sr-0.xml | grep metric\ name\=\"float_ssim\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
DE2K0=$(cat $BASENAME-vmaf-sr-0.xml | grep metric\ name\=\"ciede2000\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
VMAF0=$(cat $BASENAME-vmaf-sr-0.xml | grep metric\ name\=\"vmaf\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)

SIZE1=$(wc -c $BASENAME?1.av1 | awk '{ print $1 }')
PSNR1=$(cat $BASENAME-vmaf-sr-1.xml | grep metric\ name\=\"psnr_y\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
SSIM1=$(cat $BASENAME-vmaf-sr-1.xml | grep metric\ name\=\"float_ssim\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
DE2K1=$(cat $BASENAME-vmaf-sr-1.xml | grep metric\ name\=\"ciede2000\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
VMAF1=$(cat $BASENAME-vmaf-sr-1.xml | grep metric\ name\=\"vmaf\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)

SIZE2=$(wc -c $BASENAME?2.av1 | awk '{ print $1 }')
PSNR2=$(cat $BASENAME-vmaf-sr-2.xml | grep metric\ name\=\"psnr_y\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
SSIM2=$(cat $BASENAME-vmaf-sr-2.xml | grep metric\ name\=\"float_ssim\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
DE2K2=$(cat $BASENAME-vmaf-sr-2.xml | grep metric\ name\=\"ciede2000\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
VMAF2=$(cat $BASENAME-vmaf-sr-2.xml | grep metric\ name\=\"vmaf\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)

echo "Scaled Resolution" > $BASENAME-metrics.out
echo $SIZE0 $QP0 $PSNR0 $SSIM0 $DE2K0 $VMAF0 >> $BASENAME-metrics.out
echo $SIZE1 $QP1 $PSNR1 $SSIM1 $DE2K1 $VMAF1 >> $BASENAME-metrics.out
echo $SIZE2 $QP2 $PSNR2 $SSIM2 $DE2K2 $VMAF2 >> $BASENAME-metrics.out

  MIN_QP=1
  MAX_QP=63
  while (( $MAX_QP - $MIN_QP > 1 )); do
    QP=$(( ($MIN_QP + $MAX_QP) / 2 ))
    $LIBAOM_SVC/examples/svc_encoder_rtc $FILE $BASENAME-QP av1 $WIDTH $HEIGHT 1 24 0 0 1 1 6 $QP $QP1 $QP2 > /dev/null
    QP_SIZE=$(wc -c $BASENAME-QP?0.av1 | awk '{ print $1 }')
    echo $QP $QP_SIZE
    if (($QP_SIZE > $SIZE0)); then
      MIN_QP=$QP
      MIN_QP_SIZE=$QP_SIZE
    else
      MAX_QP=$QP
      MAX_QP_SIZE=$QP_SIZE
    fi
  done

echo $MIN_QP_SIZE $MIN_QP
echo $MAX_QP_SIZE $MAX_QP

  if (($SIZE0 - $MAX_QP_SIZE < $MIN_QP_SIZE - $SIZE0)); then
    QP0=$MAX_QP
  else
    QP0=$MIN_QP
  fi

echo "QP0 = $QP0"

  MIN_QP=1
  MAX_QP=63
  while (( $MAX_QP - $MIN_QP > 1 )); do
    QP=$(( ($MIN_QP + $MAX_QP) / 2 ))
    $LIBAOM_SVC/examples/svc_encoder_rtc $FILE $BASENAME-QP av1 $WIDTH $HEIGHT 1 24 0 0 1 1 6 $QP0 $QP $QP2 > /dev/null
    QP_SIZE=$(wc -c $BASENAME-QP?1.av1 | awk '{ print $1 }')
    echo $QP $QP_SIZE
    if (($QP_SIZE > $SIZE1)); then
      MIN_QP=$QP
      MIN_QP_SIZE=$QP_SIZE
    else
      MAX_QP=$QP
      MAX_QP_SIZE=$QP_SIZE
    fi
  done

echo $MIN_QP_SIZE $MIN_QP
echo $MAX_QP_SIZE $MAX_QP

  if (($SIZE1 - $MAX_QP_SIZE < $MIN_QP_SIZE - $SIZE1)); then
    QP1=$MAX_QP
  else
    QP1=$MIN_QP
  fi

echo "QP1 = $QP1"

  MIN_QP=1
  MAX_QP=63
  while (( $MAX_QP - $MIN_QP > 1 )); do
    QP=$(( ($MIN_QP + $MAX_QP) / 2 ))
    $LIBAOM_SVC/examples/svc_encoder_rtc $FILE $BASENAME-QP av1 $WIDTH $HEIGHT 1 24 0 0 1 1 6 $QP0 $QP1 $QP > /dev/null
    QP_SIZE=$(wc -c $BASENAME-QP?2.av1 | awk '{ print $1 }')
    echo $QP $QP_SIZE
    if (($QP_SIZE > $SIZE2)); then
      MIN_QP=$QP
      MIN_QP_SIZE=$QP_SIZE
    else
      MAX_QP=$QP
      MAX_QP_SIZE=$QP_SIZE
    fi
  done

echo $MIN_QP_SIZE $MIN_QP
echo $MAX_QP_SIZE $MAX_QP

  if (($SIZE2 - $MAX_QP_SIZE < $MIN_QP_SIZE - $SIZE2)); then
    QP2=$MAX_QP
  else
    QP2=$MIN_QP
  fi

echo "QP2 = $QP2"

$LIBAOM_SVC/examples/svc_encoder_rtc $FILE $BASENAME-QP av1 $WIDTH $HEIGHT 1 24 0 0 1 1 6 $QP0 $QP1 $QP2 > /dev/null

$LIBAOM/aomdec $BASENAME-QP?0.av1 -o $BASENAME-QP-0.y4m > /dev/null
$LIBAOM/aomdec $BASENAME-QP?1.av1 -o $BASENAME-QP-1.y4m > /dev/null
$LIBAOM/aomdec $BASENAME-QP?2.av1 -o $BASENAME-QP-2.y4m > /dev/null

# Compute metrics

$LIBVMAF/vmaf -d $BASENAME-QP-0.y4m -r $FILE -q --aom_ctc v1.0 -o $BASENAME-vmaf-sq-0.xml --xml
$LIBVMAF/vmaf -d $BASENAME-QP-1.y4m -r $FILE -q --aom_ctc v1.0 -o $BASENAME-vmaf-sq-1.xml --xml
$LIBVMAF/vmaf -d $BASENAME-QP-2.y4m -r $FILE -q --aom_ctc v1.0 -o $BASENAME-vmaf-sq-2.xml --xml

SIZE0=$(wc -c $BASENAME-QP?0.av1 | awk '{ print $1 }')
PSNR0=$(cat $BASENAME-vmaf-sq-0.xml | grep metric\ name\=\"psnr_y\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
SSIM0=$(cat $BASENAME-vmaf-sq-0.xml | grep metric\ name\=\"float_ssim\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
DE2K0=$(cat $BASENAME-vmaf-sq-0.xml | grep metric\ name\=\"ciede2000\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
VMAF0=$(cat $BASENAME-vmaf-sq-0.xml | grep metric\ name\=\"vmaf\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)

SIZE1=$(wc -c $BASENAME-QP?1.av1 | awk '{ print $1 }')
PSNR1=$(cat $BASENAME-vmaf-sq-1.xml | grep metric\ name\=\"psnr_y\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
SSIM1=$(cat $BASENAME-vmaf-sq-1.xml | grep metric\ name\=\"float_ssim\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
DE2K1=$(cat $BASENAME-vmaf-sq-1.xml | grep metric\ name\=\"ciede2000\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
VMAF1=$(cat $BASENAME-vmaf-sq-1.xml | grep metric\ name\=\"vmaf\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)

SIZE2=$(wc -c $BASENAME-QP?2.av1 | awk '{ print $1 }')
PSNR2=$(cat $BASENAME-vmaf-sq-2.xml | grep metric\ name\=\"psnr_y\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
SSIM2=$(cat $BASENAME-vmaf-sq-2.xml | grep metric\ name\=\"float_ssim\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
DE2K2=$(cat $BASENAME-vmaf-sq-2.xml | grep metric\ name\=\"ciede2000\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)
VMAF2=$(cat $BASENAME-vmaf-sq-2.xml | grep metric\ name\=\"vmaf\" | cut -d\  -f 9 | cut -d= -f 2 | cut -d\" -f 2)

echo "Scaled Quantizer" >> $BASENAME-metrics.out
echo $SIZE0 $QP0 $PSNR0 $SSIM0 $DE2K0 $VMAF0 >> $BASENAME-metrics.out
echo $SIZE1 $QP1 $PSNR1 $SSIM1 $DE2K1 $VMAF1 >> $BASENAME-metrics.out
echo $SIZE2 $QP2 $PSNR2 $SSIM2 $DE2K2 $VMAF2 >> $BASENAME-metrics.out

cat $BASENAME-metrics.out

#rm -rf *.av1 *.y4m log.txt
