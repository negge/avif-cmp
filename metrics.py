#!/usr/bin/env python

import sys

for out in sys.argv[1:]:
    f = open(out, 'r')
    metrics = f.read().split()
    print(out,' '.join(map(str, metrics[2:20])),' '.join(map(str, metrics[22:40])))
