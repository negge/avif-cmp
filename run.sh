#!/bin/bash
set -e

if [ -z "$CORES" ]; then
  if [ "$(uname -s)" = "Darwin" ]; then
    CORES=$(sysctl -n hw.ncpu)
  else
    CORES=$(grep -i processor /proc/cpuinfo | wc -l)
  fi
fi

find -L "$@" -type f -name "*.y4m" -print0 | xargs -0 -n1 -P$CORES $(dirname $0)/enc.sh
